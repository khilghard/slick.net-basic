﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;
using SlickQAClasses;

namespace SlickQABasic
{
    public partial class SlickReporting
    {
        /// <summary>
        /// Post a file to slick
        /// </summary>
        /// <param name="result"></param>
        public JsonResultsClass PostFileFromMemory(string fileContent, string slickName)
        {
            // Create PostFileResults return variable
            JsonResultsClass result = new JsonResultsClass();

            string mimeType = TestRunCreator.TestRunCreatorClass.GrabMimeTypeStatic(slickName);
            // This block of data sets up reading a binary file to slick
            var stream = GenerateStreamFromString(fileContent);
            System.IO.BinaryReader reader = new System.IO.BinaryReader(stream);
            // This object will represent the file we want to have in the server
            StoredFile file = new StoredFile();
            file.mimetype = mimeType;
            file.filename = slickName;
            file.uploadDate = SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);
            file.length = stream.Length;
            file.chunkSize = 262144;

            // This is the json serializer block to make json from a class object
            JsonSerializer serializer = new JsonSerializer();
            // Ignores null values in the json
            serializer.NullValueHandling = NullValueHandling.Ignore;

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, file);
            }

            // Setup the RestClient, here this is about setting up the file structure
            var client = new RestClient();
            client.EndPoint = String.Format(@"{0}/api/files", baseUrl);
            client.Method = HttpVerb.POST;
            client.PostData = sb.ToString();
            client.ContentType = "application/json";
            var json = client.MakeRequest();

            // Store the status and json string
            result.status = client.statusCode;
            result.jsonReturn = json;
            try
            {
                // Try to convert the string to the class object
                result.localObject = JsonConvert.DeserializeObject<StoredFile>(json);
                // Indicate that the json passed otherwise the exception block would have been called
                result.passJsonConvert = true;
            }
            catch (Exception ee)
            {
                // Oops, not json, deal with the fallout
                result.ee = ee;
                result.passJsonConvert = false;
            }

            // Make the StoredFile object data match the result from the server
            file.id = ((StoredFile)result.localObject).id;
            file.chunkSize = ((StoredFile)result.localObject).chunkSize;
            file.md5 = ((StoredFile)result.localObject).md5;
            file.mimetype = "application/octet-stream";

            // If the file creation process on the server worked, then let's add chunks to it
            if (result.status == HttpStatusCode.OK)
            {
                // Find out how many times we need to iterate the file
                Int64 remainder = 0;
                Int64 uploadChunkAttempts = Math.DivRem((Int64)file.length, file.chunkSize, out remainder);
                // Iterate through all the chunks
                for (int i = 0; i <= uploadChunkAttempts; i++)
                {
                    // Create a byte list
                    List<byte> newBytes = new List<byte>();
                    // Add the bytes from the binary file to the list
                    newBytes.AddRange(reader.ReadBytes(Convert.ToInt32(file.chunkSize)));

                    // Call the add chunk method and let the data get pushed to the server
                    PostFileAddChunk(newBytes, file.id);
                }
            }

            // Restore mimetype
            file.mimetype = mimeType;

            // Close the reader and stream
            reader.Close();
            stream.Close();

            jsonResultObject = result;
            // Return all of the result data in a list
            return result;
        }
    }
}
  