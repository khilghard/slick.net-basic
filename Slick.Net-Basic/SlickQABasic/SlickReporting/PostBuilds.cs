﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;
using SlickQAClasses;

namespace SlickQABasic
{
    public partial class SlickReporting
    {
        /// <summary>
        /// Requires a project, a testcase, a testrun, and then can add results in that order
        /// </summary>
        /// <param name="result"></param>
        public JsonResultsClass PostBuilds(SlickQAClasses.Release release, SlickQAClasses.Build build, SlickQAClasses.ProjectReference projectReference)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.NullValueHandling = NullValueHandling.Ignore;

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, build);
            }

            var client = new RestClient();
            client.EndPoint = String.Format(@"{0}/api/projects/{1}/releases/{2}/builds", baseUrl, projectReference.id, release.id);
            client.Method = HttpVerb.POST;
            client.PostData = sb.ToString();
            client.ContentType = "application/json";
            var json = client.MakeRequest();

            // Create the return variable
            JsonResultsClass result = new JsonResultsClass();
            // Store the status and json string
            result.status = client.statusCode;
            result.jsonReturn = json;
            try
            {
                // Try to convert the string to the class object
                result.localObject = JsonConvert.DeserializeObject<SlickQAClasses.Build>(json);
                result.passJsonConvert = true;
            }
            catch (Exception ee)
            {
                // Oops, not json, deal with the fallout
                result.ee = ee;
                result.passJsonConvert = false;
            }

            jsonResultObject = result;
            return result;
        }
    }
}
