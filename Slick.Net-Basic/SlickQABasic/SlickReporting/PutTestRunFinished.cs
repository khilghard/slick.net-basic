﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;
using SlickQAClasses;

namespace SlickQABasic
{
    public partial class SlickReporting
    {
        /// <summary>
        /// This is dangerous, be careful what gets updated, slick can disassociate data if you are not careful
        /// </summary>
        /// <param name="testrun"></param>
        /// <returns></returns>
        public JsonResultsClass PutTestRunFinished(SlickQAClasses.TestrunReference testRunReference, PutTestRunFinishedClass finished)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.NullValueHandling = NullValueHandling.Ignore;

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, finished);
                // {"build":{"name":"1.1.2.10784"},"dateCreated":0,"name":"Test 2015-04-04_08:51:00","project":{"name":"Panel Project"},"runFinished":0,"runStarted":0,"summary":{"total":2,"totalTime":0},"testplan":{"isprivate":false,"name":"Panel Test Plan","project":{"name":"Panel Project"}}}
            }

            var client = new RestClient();
            client.EndPoint = String.Format(@"{0}/api/testruns/{1}", baseUrl, testRunReference.testrunId);
            client.Method = HttpVerb.PUT;
            client.PostData = sb.ToString();
            client.ContentType = "application/json";
            var json = client.MakeRequest();

            // Create the return variable
            JsonResultsClass result = new JsonResultsClass();
            // Store the status and json string
            result.status = client.statusCode;
            result.jsonReturn = json;
            try
            {
                // Try to convert the string to the class object
                result.localObject = JsonConvert.DeserializeObject<SlickQAClasses.TestRunsClass.Testrun>(json);
                result.passJsonConvert = true;
            }
            catch (Exception ee)
            {
                // Oops, not json, deal with the fallout
                result.ee = ee;
                result.passJsonConvert = false;
            }

            jsonResultObject = result;
            return result;
        }

        public const string finished = "FINISHED";
        public const string toBeRun = "TO_BE_RUN";
        public const string running = "RUNNING";

        public class PutTestRunFinishedClass
        {
            public string state;
        }
    }
}
