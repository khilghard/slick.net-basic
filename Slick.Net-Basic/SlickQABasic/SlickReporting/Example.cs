﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SlickQAClasses;

namespace SlickQABasic
{
    /// <summary>
    /// Slick is setup as a reporting server and has some basic TestCase/TestPlan/Project information to relate
    /// back to TestManagement systems such as TestRail or Zephyr.
    /// 
    /// Slick requires that we first have a Project, second a TestPlan, third a Build, fourth a Release, and fifth
    /// a set of TestCases. The TestRun is created from the Project, TestPlan, Build, and Release information.
    /// Once the Project, TestPlan, Build, and Release data are added to Slick and referenced back in the TestRun, 
    /// the TestRun object can be added to Slick.
    /// 
    /// At this point a TestRun reference comes back from Slick and is used when we want to add a Result. 
    /// Results are the data of what happened when a TestCase was executed. The final data in the form of LogEntries
    /// contain the data. Other points of interest such as start and end times are added as well. Files can be added
    /// to each TestCase Result and when a failure occurs, the "reason" property can be filled with the reason for 
    /// the failure and will show in the Slick TestRun UI screen. Lastly, the run needs to be told that it is 
    /// Finished by using the Status field.
    /// 
    /// Use this example for directly entering results into Slick. Alternately use TestRunCreator in TestRunCreator
    /// to handle creating the TestRun and required objects. TestRunCreator will only upload at the end of a run
    /// and can also save the .Net object to an xml file for reference if desired.
    /// 
    /// </summary>
    public partial class Example
    {
        // The basic slick connector
        public SlickReporting slick;

        public static void RunSetup(string ipAddress)
        {
            SlickQABasic.Example testMe = new SlickQABasic.Example();
            testMe.RunSimpleSlickTest("192.168.0.44");
        }

        /// <summary>
        /// Step 1. Make a connection to the slick server
        /// Step 2. Get the project, usually just a string reference
        /// Step 3. Get the testplan, usually just a string reference
        /// Step 4. Create the test run with relevant build and release data, and testrun name
        ///         This step can also take files to upload to that testrun
        /// Step 5. 
        /// </summary>
        /// <param name="ipAddress"></param>
        public void RunSimpleSlickTest(string ipAddress)
        {
            // Create some variables
            slick = new SlickReporting(ipAddress);
            System.IO.FileInfo picture = new System.IO.FileInfo(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "Picture.jpg"));
            SlickReporting.JsonResultsClass postFileResults = slick.PostFile(picture, "Picture.jpeg", @"image/jpeg");
            SlickReporting.JsonResultsClass postFileResults1 = slick.PostFileFromMemory("Text example for new file", "Example.txt");

            SlickReporting.JsonResultsClass getFileResults = slick.GetStoredFile(postFileResults1.ObjectToStoredFile().id);

            // Grab all projects and find the one marked "Example"
            ProjectClass projectData = GetProjectReference("Example");

            // Get or Create the testplan
            SlickQAClasses.TestPlan testplan = CreateGetTestPlan(projectData, "Some test plan");

            // Create the testrun
            SlickReporting.JsonResultsClass testrunResults = CreateTestRun(projectData, "1.1.2", "1.1.2.10000", "Slick Smoke Test", new List<StoredFile>() { getFileResults.ObjectToStoredFile(), postFileResults.ObjectToStoredFile() }, testplan);

            TestrunReference testRunReference = new TestrunReference() { name = testrunResults.ObjectToTestrun().name, testrunId = testrunResults.ObjectToTestrun().id };

            #region TestCase - Add a result to the testrun
            // Results generation
            ResultClass.Result result = new ResultClass.Result();
            result.build = testrunResults.ObjectToTestrun().build;
            result.hostname = "COMPUTER";
            result.log = new List<ResultClass.LogEntry>() {
                new ResultClass.LogEntry() { loggerName = "Entry", message = "Hello World 1" },
                new ResultClass.LogEntry() { loggerName = "Entry", message = "Hello World 2" }
            };
            result.project = projectData.projectReference;
            result.runstatus = "Passed";
            result.testcase = CreateGetTestCase(projectData.projectReference, "Test Me 1");
            result.testrun = testRunReference;
            result.status = "PASS";
            result.files = new List<StoredFile>();
            // result.files.Add(postFileResults.ObjectToStoredFile());
            result.started = SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);
            result.finished = SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);
            result.recorded = SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);
            // POST the results
            SlickReporting.JsonResultsClass testResults1 = slick.PostResults(result);
            #endregion

            #region TestCase - Add another result
            // Results generation
            result = new ResultClass.Result();
            result.build = testrunResults.ObjectToTestrun().build;
            result.hostname = "COMPUTER";
            result.log = new List<ResultClass.LogEntry>() {
                new ResultClass.LogEntry() { loggerName = "Entry", message = "Hello World 3" },
                new ResultClass.LogEntry() { loggerName = "Entry", message = "Hello World 4" }
            };
            result.project = projectData.projectReference;
            result.runstatus = "Passed";
            result.testcase = CreateGetTestCase(projectData.projectReference, "Test Me 2");
            result.testrun = testRunReference;
            result.status = "PASS";
            result.started = SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);
            result.finished = SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);
            result.recorded = SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);
            // POST the results
            SlickReporting.JsonResultsClass testResults2 = slick.PostResults(result);

            #endregion

            // Report FINISHED to Slick for E-mail push
            SlickQABasic.SlickReporting.JsonResultsClass testRunResultsGet = slick.GetTestRun(testRunReference);

            SlickQABasic.SlickReporting.PutTestRunFinishedClass finished = new SlickQABasic.SlickReporting.PutTestRunFinishedClass() { state = SlickQABasic.SlickReporting.finished };
            SlickQABasic.SlickReporting.JsonResultsClass testRunResultsPut = slick.PutTestRunFinished(testRunReference, finished);
        }

        /// <summary>
        /// Grab a project reference
        /// </summary>
        /// <returns></returns>
        private ProjectClass GetProjectReference(string name)
        {
            ProjectClass projectClassData = new ProjectClass();
            // Setup the project
            projectClassData.project.name = name;
            // Get slick projects (Currently broken by a parse error on the server
            SlickQABasic.SlickReporting.JsonResultsClass projectResults = slick.GetProject(projectClassData.project.name);
            // Make sure to assign the project data to a variable to be passed out of the method
            projectClassData.project = projectResults.ObjectToProject();

            // Now we get the reference id of the project and make a project reference
            projectClassData.projectReference.name = projectClassData.project.name;
            if (projectResults.passJsonConvert)
            {
                projectClassData.projectReference.id = projectResults.ObjectToProject().id;
            }

            // Return the project and project reference
            return projectClassData;
        }

        private SlickQABasic.SlickReporting.JsonResultsClass CreateTestRun(ProjectClass projectData, string Release, string Build, string testRunName, List<SlickQAClasses.StoredFile> storedFilesList, SlickQAClasses.TestPlan testplan)
        {
            SlickQAClasses.TestRunsClass.Testrun testrun = new SlickQAClasses.TestRunsClass.Testrun();
            string dateString = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            testrun.name = testRunName;
            testrun.project = projectData.projectReference;
            testrun.testplan = testplan;
            testrun.testplanId = testplan.id;

            // Attach any data about the relase to the testrun
            if (projectData.project.releases.Count > 0)
            {
                // Indicate what we found
                bool foundRelease = false;

                for (int i = 0; i < projectData.project.releases.Count; i++)
                {
                    if (projectData.project.releases[i].name == Release)
                    {
                        // Create a release reference to use
                        SlickQAClasses.ReleaseReference releaseReference = new SlickQAClasses.ReleaseReference();
                        releaseReference.name = projectData.project.releases[i].name;
                        releaseReference.releaseId = projectData.project.releases[i].id;

                        // Create the release full data
                        SlickQAClasses.Release releaseObject = projectData.project.releases[i];

                        // Attach the reference
                        testrun.release = releaseReference;

                        // Indicate what we found
                        bool foundBuild = false;

                        // Iterate all builds
                        for (int j = 0; j < projectData.project.releases[i].builds.Count; j++)
                        {
                            // Check for the build name that the ATML is reporting
                            if (projectData.project.releases[i].builds[j].name == Build)
                            {
                                // Create the build data
                                testrun.build = new SlickQAClasses.BuildReference() { name = projectData.project.releases[i].builds[j].name, buildId = projectData.project.releases[i].builds[j].id };

                                // Yeah!
                                foundBuild = true;

                                // Leave the build loop
                                break;
                            }
                        }

                        // If no build, then make one
                        if (!foundBuild)
                        {
                            // Add the build to the project
                            SlickReporting.JsonResultsClass buildResults = slick.PostBuilds(releaseObject, new SlickQAClasses.Build() { name = Build }, projectData.projectReference);

                            // Add the build to the testrun
                            testrun.build = new SlickQAClasses.BuildReference() { name = buildResults.ObjectToBuild().name, buildId = buildResults.ObjectToBuild().id };
                        }

                        foundRelease = true;

                        // Leave the loop
                        break;
                    }
                }

                if (!foundRelease)
                {
                    // Add the release and build data
                    SlickReporting.JsonResultsClass releasesResults = slick.PostReleases(new SlickQAClasses.Release() { name = Release }, projectData.projectReference);
                    // Add the build to the project
                    SlickReporting.JsonResultsClass buildResults = slick.PostBuilds(releasesResults.ObjectToRelease(), new SlickQAClasses.Build() { name = Build }, projectData.projectReference);

                    // Add the reference data above to the testrun
                    testrun.release = new SlickQAClasses.ReleaseReference() { name = releasesResults.ObjectToRelease().name, releaseId = releasesResults.ObjectToRelease().id };
                    testrun.build = new SlickQAClasses.BuildReference() { name = buildResults.ObjectToBuild().name, buildId = buildResults.ObjectToBuild().id };
                }
            }

            testrun.dateCreated = SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);

            /// Add files to a testrun
            // Check for null
            if(storedFilesList != null)
            {
                // Create the files list
                testrun.files = new List<StoredFile>();
                testrun.files.AddRange(storedFilesList);
            }

            // POST the testrun
            SlickQABasic.SlickReporting.JsonResultsClass postTestRunResults = slick.PostTestRun(testrun);

            // Create the testrun reference
            SlickQAClasses.TestrunReference testRunReference = new SlickQAClasses.TestrunReference();
            testRunReference.name = testrun.name;
            if (postTestRunResults.passJsonConvert)
            {
                testRunReference.testrunId = postTestRunResults.ObjectToTestrun().id;
            }

            return postTestRunResults;
        }

        private SlickQAClasses.TestPlan CreateGetTestPlan(ProjectClass projectData, string testPlanName)
        {
            TestPlan testplan = new TestPlan();
            SlickReporting.JsonResultsClass postTestPlanResults = new SlickReporting.JsonResultsClass();
            SlickReporting.JsonResultsClass testPlanResults = slick.GetTestPlan();
            if (testPlanResults.ObjectToGetTestPlan().Count == 0)
            {
                // POST a test plan
                testplan.name = testPlanName;
                testplan.createdBy = Environment.UserName;
                testplan.project = projectData.projectReference;
                postTestPlanResults = slick.PostTestPlan(testplan);

                // Add all values from slick to our local testplan
                testplan = postTestPlanResults.ObjectToTestPlan();
            }
            else
            {
                // Indicate what we found
                bool foundPlan = false;

                // Iterate all plans
                for (int i = 0; i < testPlanResults.ObjectToGetTestPlan().Count; i++)
                {
                    // Try to find a match
                    if (testPlanResults.ObjectToGetTestPlan()[i].name == testPlanName)
                    {
                        testplan = testPlanResults.ObjectToGetTestPlan()[i];
                        foundPlan = true;
                    }
                }

                // If no plan, then go ahead and create it
                if (!foundPlan)
                {
                    // POST a test plan
                    testplan.name = testPlanName;
                    testplan.createdBy = Environment.UserName;
                    testplan.project = projectData.projectReference;
                    postTestPlanResults = slick.PostTestPlan(testplan);

                    // Add all values from slick to our local testplan
                    testplan = postTestPlanResults.ObjectToTestPlan();
                }
            }

            return testplan;
        }

        /// <summary>
        /// This will grab a test case out of slick or add one and the return the id reference
        /// </summary>
        /// <param name="projectReference">The project we need to reference</param>
        /// <param name="testCaseString">The name of the test case, it's how we get the testcase id reference</param>
        /// <returns></returns>
        private SlickQAClasses.TestcaseReference CreateGetTestCase(SlickQAClasses.ProjectReference projectReference, string testCaseString)
        {
            // slickSetup.testcase_list.testcase[slickSetup.testcase_list.default_index].name
            SlickQAClasses.TestCaseClass.Testcase testCase = new SlickQAClasses.TestCaseClass.Testcase();
            testCase.name = testCaseString;
            testCase.project = projectReference;
            testCase.automated = true;
            SlickQABasic.SlickReporting.JsonResultsClass getResults = slick.GetTestCases();

            // Create the testcase name
            SlickQAClasses.TestcaseReference testCaseReference = new SlickQAClasses.TestcaseReference();
            testCaseReference.name = testCase.name;

            // Now check to see if that testcase already exists
            // If not create it
            if (getResults.ObjectToGetTestCases().Count == 0)
            {
                SlickQABasic.SlickReporting.JsonResultsClass testCasePostResults = slick.PostTestCase(testCase);

                if (testCasePostResults.passJsonConvert)
                {
                    testCaseReference.testcaseId = testCasePostResults.ObjectToTestCase().id;
                }
            }
            else
            {
                // We have a list, now itereate the list and check for the testcase name
                for (int i = 0; i < getResults.ObjectToGetTestCases().Count; i++)
                {
                    if (getResults.passJsonConvert)
                    {
                        if (getResults.ObjectToGetTestCases()[i].name == testCase.name)
                        {
                            testCaseReference.testcaseId = getResults.ObjectToGetTestCases()[i].id;
                            break;
                        }
                    }
                }
            }

            // If no testcase from the get list, then we need to post this one
            if (testCaseReference.testcaseId == "")
            {
                SlickQABasic.SlickReporting.JsonResultsClass testCasePostResults = slick.PostTestCase(testCase);

                if (testCasePostResults.passJsonConvert)
                {
                    testCaseReference.testcaseId = testCasePostResults.ObjectToTestCase().id;
                }
            }

            return testCaseReference;
        }

        public class ProjectClass
        {
            public SlickQAClasses.ProjectsClass.Project project = new SlickQAClasses.ProjectsClass.Project();
            public SlickQAClasses.ProjectReference projectReference = new SlickQAClasses.ProjectReference();
        }
    }
}