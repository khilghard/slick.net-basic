﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;
using SlickQAClasses;

namespace SlickQABasic
{
    public partial class SlickReporting
    {
        public class JsonResultsClass : SlickAction
        {
            public bool passJsonConvert;
            public string jsonReturn;
            public HttpStatusCode status;
            public object localObject;
            public Exception ee;

            public List<TestCaseClass.Testcase> ObjectToGetTestCases()
            {
                return (List<TestCaseClass.Testcase>)localObject;
            }

            public List<SlickQAClasses.TestPlan> ObjectToGetTestPlan()
            {
                return (List<SlickQAClasses.TestPlan>)localObject;
            }

            public SlickQAClasses.TestRunsClass.Testrun ObjectToGetTestRun()
            {
                return (SlickQAClasses.TestRunsClass.Testrun)localObject;
            }

            public List<SlickQAClasses.TestRunsClass.Testrun> ObjectToGetTestRuns()
            {
                return (List<SlickQAClasses.TestRunsClass.Testrun>)localObject;
            }

            public SlickQAClasses.Build ObjectToBuild()
            {
                return (SlickQAClasses.Build)localObject;
            }

            public StoredFile ObjectToStoredFile()
            {
                return (StoredFile)localObject;
            }

            public ProjectsClass.Project ObjectToProject()
            {
                return (ProjectsClass.Project)localObject;
            }

            public SlickQAClasses.ProjectsClass.Component ObjectToComponent()
            {
                return (SlickQAClasses.ProjectsClass.Component)localObject;
            }

            public SlickQAClasses.Release ObjectToRelease()
            {
                return (SlickQAClasses.Release)localObject;
            }

            public List<SlickQAClasses.Release> ObjectToReleaseList()
            {
                return (List<SlickQAClasses.Release>)localObject;
            }

            public ResultClass.Result ObjectToResult()
            {
                return (ResultClass.Result)localObject;
            }

            public TestCaseClass.Testcase ObjectToTestCase()
            {
                return (TestCaseClass.Testcase)localObject;
            }

            public SlickQAClasses.TestPlan ObjectToTestPlan()
            {
                return (SlickQAClasses.TestPlan)localObject;
            }

            public TestRunsClass.Testrun ObjectToTestrun()
            {
                return (TestRunsClass.Testrun)localObject;
            }

            public List<SlickQAClasses.Build> ObjectToBuildList()
            {
                return (List<SlickQAClasses.Build>)localObject;
            }
        }
    }
}