﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

namespace TestRunCreator
{
    /// <summary>
    /// Consider this the top level class to adding a testrun with results to slick.
    /// Everything in this class is needed in order for slick to look "right".
    /// </summary>
    public partial class TestRunCreatorClass
    {
        /// <summary>
        /// Get or create a test case
        /// </summary>
        /// <param name="testCaseString">Name of the test case</param>
        /// <param name="component">This groups the testcases together in the UI</param>
        /// <returns>Returns a reference to the test case</returns>
        public SlickQAClasses.TestcaseReference CreateGetTestCase(string testCaseString, string component)
        {
            // Get the project reference from the TestRunActual
            SlickQAClasses.ProjectReference projectReference = TestRunActual.project;

            // Get the test case or create it with this method
            return CreateGetTestCase(projectReference, testCaseString, component);
        }

        /// <summary>
        /// This will grab a test case out of slick or add one and the return the id reference
        /// </summary>
        /// <param name="projectReference">The project we need to reference</param>
        /// <param name="testCaseString">The name of the test case, it's how we get the testcase id reference</param>
        /// <returns></returns>
        private SlickQAClasses.TestcaseReference CreateGetTestCase(SlickQAClasses.ProjectReference projectReference, string testCaseString, string component)
        {
            // slickSetup.testcase_list.testcase[slickSetup.testcase_list.default_index].name
            SlickQAClasses.TestCaseClass.Testcase testCase = new SlickQAClasses.TestCaseClass.Testcase();
            testCase.name = testCaseString;
            testCase.project = projectReference;
            testCase.automated = true;
            testCase.author = System.Environment.UserName;

            bool useComponent = false;
            if (component != null)
            {
                if (component != "")
                {
                    bool foundComponent = false;
                    for (int i = 0; i < componentList.Count; i++)
                    {
                        if (componentList[i].name == component)
                        {
                            SlickQAClasses.TestCaseClass.ComponentReference componentReference = new SlickQAClasses.TestCaseClass.ComponentReference();
                            componentReference.code = componentList[i].code;
                            componentReference.id = componentList[i].id;
                            componentReference.name = componentList[i].name;

                            testCase.component = componentReference;
                            foundComponent = true;
                            useComponent = true;
                        }
                    }

                    if (!foundComponent)
                    {
                        // Get the project reference
                        ProjectClass projectData = GetProjectReference(TestRunActual.project.id);
                        SlickQAClasses.ProjectsClass.Component littleComponent = new SlickQAClasses.ProjectsClass.Component(){ name = component, code = component.ToLower().Replace(' ', '-') };

                        SlickQABasic.SlickReporting.JsonResultsClass componentResults = slickBasic.PostProjectComponentMethod(projectData.project.id, littleComponent);
                        
                        // Get new project data and reference it back to the TestRun
                        projectData = GetProjectReference(TestRunActual.project.id);
                        TestRunActual.project = projectData.projectReference;
                        componentList = projectData.project.components;

                        testCase.component = new SlickQAClasses.TestCaseClass.ComponentReference() { name = componentResults.ObjectToComponent().name, id = componentResults.ObjectToComponent().id, code = componentResults.ObjectToComponent().code };
                        useComponent = true;
                    }
                }
            }

            SlickQABasic.SlickReporting.JsonResultsClass getResults = slickBasic.GetTestCases();

            // Create the testcase name
            SlickQAClasses.TestcaseReference testCaseReference = new SlickQAClasses.TestcaseReference();
            testCaseReference.name = testCase.name;

            // Now check to see if that testcase already exists
            // If not create it
            if (getResults.ObjectToGetTestCases().Count == 0)
            {
                SlickQABasic.SlickReporting.JsonResultsClass testCasePostResults = slickBasic.PostTestCase(testCase);

                if (testCasePostResults.passJsonConvert)
                {
                    testCaseReference.testcaseId = testCasePostResults.ObjectToTestCase().id;
                }
            }
            else
            {
                // We have a list, now itereate the list and check for the testcase name
                for (int i = 0; i < getResults.ObjectToGetTestCases().Count; i++)
                {
                    if (getResults.passJsonConvert)
                    {
                        if (getResults.ObjectToGetTestCases()[i].name == testCase.name)
                        {
                            #region Verify component attachment
                            if (useComponent)
                            {
                                for (int componentIndex = 0; componentIndex < componentList.Count; componentIndex++)
                                {
                                    if(component == componentList[componentIndex].name)
                                    {
                                        if (getResults.ObjectToGetTestCases()[i].component == null)
                                        {
                                            getResults.ObjectToGetTestCases()[i].component = new SlickQAClasses.TestCaseClass.ComponentReference() { name = componentList[componentIndex].name, id = componentList[componentIndex].id, code = componentList[componentIndex].code };

                                            slickBasic.PutTestCase(getResults.ObjectToGetTestCases()[i]);
                                            break;
                                        }
                                    }
                                }
                            
                                // Get new project data and reference it back to the TestRun
                                ProjectClass projectData = GetProjectReference(TestRunActual.project.id);
                                TestRunActual.project = projectData.projectReference;
                                componentList = projectData.project.components;
                            }
                            #endregion

                            testCaseReference.testcaseId = getResults.ObjectToGetTestCases()[i].id;
                            break;
                        }
                    }
                }
            }

            // If no testcase from the get list, then we need to post this one
            if (testCaseReference.testcaseId == "" | testCaseReference.testcaseId == null)
            {
                SlickQABasic.SlickReporting.JsonResultsClass testCasePostResults = slickBasic.PostTestCase(testCase);

                if (testCasePostResults.passJsonConvert)
                {
                    testCaseReference.testcaseId = testCasePostResults.ObjectToTestCase().id;
                }
            }

            return testCaseReference;
        }
    }
}