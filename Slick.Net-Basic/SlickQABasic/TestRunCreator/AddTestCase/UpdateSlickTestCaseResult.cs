﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

namespace TestRunCreator
{
    public partial class TestRunCreatorClass
    {
        public bool UpdateResultAll(
            SlickQAClasses.ResultClass.Result result,
            DateTime endTestDateTime,
            DateTime startTestDateTime,
            List<SlickQAClasses.ResultClass.LogEntry> logEntriesList,
            SlickStringResults slickResult,
            List<FileClass> localFilesList = null,
            string reasonData = null)
        {
            return UpdateResultAll(result, endTestDateTime, startTestDateTime, logEntriesList, slickResult, localFilesList, reasonData, result.hostname);
        }

        public bool UpdateResultAll(
            SlickQAClasses.ResultClass.Result result,
            DateTime endTestDateTime,
            DateTime startTestDateTime,
            List<SlickQAClasses.ResultClass.LogEntry> logEntriesList,
            SlickStringResults slickResult,
            List<FileClass> localFilesList = null,
            string reasonData = null,
            string hostName = null)
        {
            // Add remaining data
            if (hostName == null)
            {
                result.hostname = Environment.MachineName;
            }
            else
            {
                result.hostname = hostName;
            }

            /// This will get updated after the testrun is posted, each result
            /// will then get the TestRunReference data from slick and then get
            /// updated prior to the result POST
            result.testrun = new SlickQAClasses.TestrunReference() { name = TestRunActual.name, testrunId = "" };
            result.finished = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(endTestDateTime);
            result.started = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(startTestDateTime);
            result.runlength = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(endTestDateTime) - SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(startTestDateTime);
            result.log = new List<SlickQAClasses.ResultClass.LogEntry>();
            result.recorded = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);

            if (reasonData != null)
            {
                result.reason = reasonData;
            }

            if (localFilesList != null)
            {
                if (localFilesList.Count > 0)
                {
                    // Setup the files list
                    result.files = new List<SlickQAClasses.StoredFile>();

                    // Add all files to the list
                    for (int fileQ = 0; fileQ < localFilesList.Count; fileQ++)
                    {
                        if (localFilesList[fileQ].id == null)
                        {
                            // The automation deployment system file local
                            System.IO.FileInfo file = new System.IO.FileInfo(localFilesList[fileQ].LocalFileName);
                            // POST the file
                            if (file.Exists)
                            {
                                SlickQABasic.SlickReporting.JsonResultsClass posted = slickBasic.PostFile(localFilesList[fileQ].LocalFileName, localFilesList[fileQ].SlickFileName, GrabMimeType(System.IO.Path.GetExtension(localFilesList[fileQ].LocalFileName)));
                                result.files.Add((SlickQAClasses.StoredFile)posted.localObject);
                            }
                        }
                        else
                        {
                            if (localFilesList[fileQ].id.Length > 0)
                            {
                                SlickQABasic.SlickReporting.JsonResultsClass resultFileGet = slickBasic.GetStoredFile(localFilesList[fileQ].id);
                                result.files.Add(resultFileGet.ObjectToStoredFile());
                            }
                        }
                    }
                }
            }

            result.log = logEntriesList;

            // Pass the list blob to this method to find what the overall result is
            result.status = SlickStringConvert(slickResult);

            return true;
        }

        public void UpdateResultEndTime(SlickQAClasses.ResultClass.Result result, DateTime endTestDateTime)
        {
            result.finished = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(endTestDateTime);
            result.runlength = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(endTestDateTime) - result.started;
            if (result.runlength < 0)
            {
                result.runlength = 0;
            }
        }

        public void UpdateResultStartTime(SlickQAClasses.ResultClass.Result result, DateTime startTestDateTime)
        {
            result.started = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(startTestDateTime);
            result.runlength = result.finished - SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(startTestDateTime);
            if (result.runlength < 0)
            {
                result.runlength = 0;
            }
        }

        public void UpdateResultLogEntries(SlickQAClasses.ResultClass.Result result, List<SlickQAClasses.ResultClass.LogEntry> logEntriesList)
        {
            if (result.log != null)
            {
                result.log.AddRange(logEntriesList);
            }
            else
            {
                result.log = logEntriesList;
            }
        }

        public void UpdateResultStatus(SlickQAClasses.ResultClass.Result result, SlickStringResults slickResult)
        {
            result.status = SlickStringConvert(slickResult);
        }

        public void UpdateResultFiles(
            SlickQAClasses.ResultClass.Result result,
            List<FileClass> localFilesList)
        {
            if (result.files != null)
            {
                result.files = new List<SlickQAClasses.StoredFile>();
            }

            // Add all files to the list
            for (int fileQ = 0; fileQ < localFilesList.Count; fileQ++)
            {
                if (localFilesList[fileQ].id == null)
                {
                    // The automation deployment system file local
                    System.IO.FileInfo file = new System.IO.FileInfo(localFilesList[fileQ].LocalFileName);
                    // POST the file
                    if (file.Exists)
                    {
                        SlickQABasic.SlickReporting.JsonResultsClass posted = slickBasic.PostFile(localFilesList[fileQ].LocalFileName, localFilesList[fileQ].SlickFileName, GrabMimeType(System.IO.Path.GetExtension(localFilesList[fileQ].LocalFileName)));
                        result.files.Add((SlickQAClasses.StoredFile)posted.localObject);
                    }
                }
                else
                {
                    if (localFilesList[fileQ].id.Length > 0)
                    {
                        SlickQABasic.SlickReporting.JsonResultsClass resultFileGet = slickBasic.GetStoredFile(localFilesList[fileQ].id);
                        result.files.Add(resultFileGet.ObjectToStoredFile());
                    }
                }
            }
        }

        public void UpdateResultReason(
            SlickQAClasses.ResultClass.Result result,
            string reasonData)
        {
            result.reason = reasonData;
        }

        public void UpdateResultHostname(
            SlickQAClasses.ResultClass.Result result,
            string hostName
            )
        {
            result.hostname = hostName;
        }
    }
}