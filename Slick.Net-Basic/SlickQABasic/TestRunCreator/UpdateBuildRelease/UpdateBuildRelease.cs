﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

namespace TestRunCreator
{
    /// <summary>
    /// Consider this the top level class to adding a testrun with results to slick.
    /// Everything in this class is needed in order for slick to look "right".
    /// </summary>
    public partial class TestRunCreatorClass
    {
        /// <summary>
        /// This will update any build and release data in the TestRun
        /// </summary>
        /// <param name="Build">Build getting tested, e.g. 2.0.0.13188</param>
        /// <param name="Release">Release getting tested, e.g. 2.0.0</param>
        public void UpdateBuildRelease(string Build, string Release)
        {
            if (TestRunActual != null)
            {
                // Get the project reference
                ProjectClass projectData = GetProjectReference(TestRunActual.project.name);

                // Attach any data about the relase to the testrun
                if (projectData.project.releases.Count > 0)
                {
                    // Indicate what we found
                    bool foundRelease = false;

                    for (int i = 0; i < projectData.project.releases.Count; i++)
                    {
                        if (projectData.project.releases[i].name == Release)
                        {
                            // Create a release reference to use
                            SlickQAClasses.ReleaseReference releaseReference = new SlickQAClasses.ReleaseReference();
                            releaseReference.name = projectData.project.releases[i].name;
                            releaseReference.releaseId = projectData.project.releases[i].id;

                            // Create the release full data
                            SlickQAClasses.Release releaseObject = projectData.project.releases[i];

                            // Attach the reference
                            TestRunActual.release = releaseReference;

                            // Indicate what we found
                            bool foundBuild = false;

                            // Iterate all builds
                            for (int j = 0; j < projectData.project.releases[i].builds.Count; j++)
                            {
                                // Check for the build name that the ATML is reporting
                                if (projectData.project.releases[i].builds[j].name == Build)
                                {
                                    // Create the build data
                                    TestRunActual.build = new SlickQAClasses.BuildReference() { name = projectData.project.releases[i].builds[j].name, buildId = projectData.project.releases[i].builds[j].id };

                                    // Yeah!
                                    foundBuild = true;

                                    // Leave the build loop
                                    break;
                                }
                            }

                            // If no build, then make one
                            if (!foundBuild)
                            {
                                // Add the build to the project
                                SlickQABasic.SlickReporting.JsonResultsClass buildResults = slickBasic.PostBuilds(releaseObject, new SlickQAClasses.Build() { name = Build }, projectData.projectReference);

                                // Add the build to the testrun
                                TestRunActual.build = new SlickQAClasses.BuildReference() { name = buildResults.ObjectToBuild().name, buildId = buildResults.ObjectToBuild().id };
                            }

                            foundRelease = true;

                            // Leave the loop
                            break;
                        }
                    }

                    if (!foundRelease)
                    {
                        // Add the release and build data
                        SlickQABasic.SlickReporting.JsonResultsClass releasesResults = slickBasic.PostReleases(new SlickQAClasses.Release() { name = Release }, projectData.projectReference);
                        // Add the build to the project
                        SlickQABasic.SlickReporting.JsonResultsClass buildResults = slickBasic.PostBuilds(releasesResults.ObjectToRelease(), new SlickQAClasses.Build() { name = Build }, projectData.projectReference);

                        // Add the reference data above to the testrun
                        TestRunActual.release = new SlickQAClasses.ReleaseReference() { name = releasesResults.ObjectToRelease().name, releaseId = releasesResults.ObjectToRelease().id };
                        TestRunActual.build = new SlickQAClasses.BuildReference() { name = buildResults.ObjectToBuild().name, buildId = buildResults.ObjectToBuild().id };
                    }
                }
            }
        }
    }
}