﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

namespace TestRunCreator
{
    /// <summary>
    /// Consider this the top level class to adding a testrun with results to slick.
    /// Everything in this class is needed in order for slick to look "right".
    /// </summary>
    public partial class TestRunCreatorClass
    {
        private SlickQAClasses.TestRunsClass.Testrun CreateTestRunActual(ProjectClass projectData, string testRunName, string Build, string Release,  SlickQAClasses.TestPlan testplan, string environment)
        {
            SlickQAClasses.TestRunsClass.Testrun testrun = new SlickQAClasses.TestRunsClass.Testrun();
            string dateString = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            testrun.name = testRunName;
            testrun.project = projectData.projectReference;
            testrun.testplan = testplan;
            testrun.testplanId = testplan.id;
            testrun.state = SlickQABasic.SlickReporting.running;
            testrun.config = new SlickQAClasses.TestRunsClass.ConfigurationReference() { name = environment };

            SlickQABasic.SlickReporting.JsonResultsClass projectResults = slickBasic.GetProject(projectData.projectReference.name);

            SlickQABasic.SlickReporting.JsonResultsClass releasesGet = slickBasic.GetReleases(projectData.projectReference);
            bool foundRelease = false;
            SlickQAClasses.Release releaseObject = null;
            for (int i = 0; i < releasesGet.ObjectToReleaseList().Count; i++)
            {
                if (releasesGet.ObjectToReleaseList()[i].name == Release)
                {
                    // Create a release reference to use
                    SlickQAClasses.ReleaseReference releaseReference = new SlickQAClasses.ReleaseReference();
                    releaseReference.name = releasesGet.ObjectToReleaseList()[i].name;
                    releaseReference.releaseId = releasesGet.ObjectToReleaseList()[i].id;

                    // Attach the reference
                    testrun.release = releaseReference;

                    releaseObject = releasesGet.ObjectToReleaseList()[i];

                    foundRelease = true;
                    break;
                }
            }

            // Upload valid data only
            if (!foundRelease & Release != "")
            {
                // Add the release and build data
                SlickQABasic.SlickReporting.JsonResultsClass releasesResults = slickBasic.PostReleases(new SlickQAClasses.Release() { name = Release, target = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(DateTime.Now) }, projectData.projectReference);

                // Add the reference data above to the testrun
                testrun.release = new SlickQAClasses.ReleaseReference() { name = releasesResults.ObjectToRelease().name, releaseId = releasesResults.ObjectToRelease().id };

                releaseObject = releasesResults.ObjectToRelease();
            }

            // Upload valid data only
            SlickQAClasses.Build buildObject = null;
            if (releaseObject != null)
            {
                SlickQABasic.SlickReporting.JsonResultsClass buildsGet = slickBasic.GetBuilds(releaseObject, testrun.project);
                bool foundBuild = false;
                for (int i = 0; i < buildsGet.ObjectToBuildList().Count; i++)
                {
                    if (buildsGet.ObjectToBuildList()[i].name == Build)
                    {
                        // Create the build data
                        testrun.build = new SlickQAClasses.BuildReference() { name = buildsGet.ObjectToBuildList()[i].name, buildId = buildsGet.ObjectToBuildList()[i].id };

                        // Yeah!
                        foundBuild = true;

                        buildObject = buildsGet.ObjectToBuildList()[i];
                        break;
                    }
                }

                if (!foundBuild & Build != "")
                {
                    // Add the build to the project
                    SlickQABasic.SlickReporting.JsonResultsClass buildResults = slickBasic.PostBuilds(releaseObject, new SlickQAClasses.Build() { name = Build, built = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(DateTime.Now) }, projectData.projectReference);

                    // Add the build to the testrun
                    testrun.build = new SlickQAClasses.BuildReference() { name = buildResults.ObjectToBuild().name, buildId = buildResults.ObjectToBuild().id };

                    buildObject = buildResults.ObjectToBuild();
                }
            }

            testrun.runStarted = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);
            testrun.dateCreated = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);

            return testrun;
        }
    }
}