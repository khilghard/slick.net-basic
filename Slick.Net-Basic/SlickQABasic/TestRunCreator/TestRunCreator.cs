﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

namespace TestRunCreator
{
    /// <summary>
    /// Consider this the top level class to adding a testrun with results to slick.
    /// Everything in this class is needed in order for slick to look "right".
    /// 
    /// Process to creating a TestRun:
    /// 1. Instantiate TestRunCreator with a reference to SlickQABasic.SlickReporting
    /// 2. Call CreateTestRun
    /// 3. For each TestCase, call AddSlickTestCaseResult
    ///    a. For a log entry use call CreateLogEntry, Log entries are used to 
    ///       provide detail about the results of the TestCase and can include
    ///       exception handling details
    /// 4. When finished, call PostTestRun
    /// 
    /// Can also call AddFilesToTestRun to add a file to the overall TestRun
    /// </summary>
    public partial class TestRunCreatorClass
    {
        public SlickQAClasses.TestRunsClass.Testrun TestRunActual { get; set; }
        public SlickQAClasses.TestrunReference TestRunReferenceActual { get; set; }
        public List<SlickQAClasses.ResultClass.Result> ResultList = new List<SlickQAClasses.ResultClass.Result>();
        private SlickQABasic.SlickReporting slickBasic { get; set; }
        private string dateOfTest = "";
        public string ReportFile { get; set; }
        private const string rev = "001";
        private List<SlickQAClasses.ProjectsClass.Component> componentList = new List<SlickQAClasses.ProjectsClass.Component>();

        public TestRunCreatorClass()
        {
            /// Just create a base report for production slick, this is not the right
            /// place to do this, create this object and then set the slickBasic
            /// property afer instantiating this class. Use the other parameter based
            /// TestRunCreator constructor object instead.
            /// This parameterless constructor is here for xml serialization. It won't
            /// work otherwise. You have been warned!
            slickBasic = new SlickQABasic.SlickReporting("slickqa.vivint.com");
        }

        public TestRunCreatorClass(SlickQABasic.SlickReporting _slickBasic)
        {
            slickBasic = _slickBasic;

            ReportCreateFileName();

            // Make sure we have a report folder to dump into
            if (!System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(ReportFile)))
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(ReportFile));
            }
        }

        /// <summary>
        /// Here we create a testrun against the slick server
        /// </summary>
        /// <param name="projectName">Name of the Project</param>
        /// <param name="testPlanName">Name of the TestPlan</param>
        /// <param name="testRunName">Name of the TestRun</param>
        /// <param name="Build">Build number such as 2.0.0.13188</param>
        /// <param name="Release">Release number such as 2.0.0</param>
        /// <param name="environment">The environment that the test ran on</param>
        public void CreateTestRun(string projectName, string testPlanName, string testRunName, string Build, string Release, string environment)
        {
            // Get the project reference
            ProjectClass projectData = GetProjectReference(projectName);

            // Grab all of the components
            componentList = projectData.project.components;

            // Get the testplan
            SlickQAClasses.TestPlan testPlanSlick = CreateGetTestPlan(projectData, testPlanName);

            // Create the test run
            TestRunActual = CreateTestRunActual(projectData, testRunName, Build, Release, testPlanSlick, environment);
        }

        /// <summary>
        /// Here we post the gathered data at the end of the run.
        /// All files are added as the test is run so that slick
        /// generates the unique id's instead of the test program.
        /// </summary>
        public void PostTestRun()
        {
            // Indicate when the run is finished
            TestRunActual.runFinished = SlickQABasic.SlickReporting.ConvertDatetimeToEpoch(DateTime.Now);

            // Post the test run
            SlickQABasic.SlickReporting.JsonResultsClass postTestRunResults = slickBasic.PostTestRun(TestRunActual);

            // Create the test run reference
            TestRunReferenceActual = new SlickQAClasses.TestrunReference();
            TestRunReferenceActual.name = TestRunActual.name;
            if (postTestRunResults.passJsonConvert)
            {
                // Here we grab the reference id from the post
                TestRunReferenceActual.testrunId = postTestRunResults.ObjectToTestrun().id;
            }

            // Now add each result from each TestCase
            for(int i = 0; i < ResultList.Count; i++)
            {
                // Update the test reference for each result, they won't match after the TestRun is posted
                ResultList[i].testrun = TestRunReferenceActual;
                // POST the result
                SlickQABasic.SlickReporting.JsonResultsClass testResults1 = slickBasic.PostResults(ResultList[i]);
            }

            // Report FINISHED to Slick for E-mail notification
            SlickQABasic.SlickReporting.JsonResultsClass testRunResultsGet = slickBasic.GetTestRun(TestRunReferenceActual);

            SlickQABasic.SlickReporting.PutTestRunFinishedClass finished = new SlickQABasic.SlickReporting.PutTestRunFinishedClass() { state = SlickQABasic.SlickReporting.finished };
            // PUT the new finished state
            SlickQABasic.SlickReporting.JsonResultsClass testRunResultsPut = slickBasic.PutTestRunFinished(TestRunReferenceActual, finished);
        }
    }
}