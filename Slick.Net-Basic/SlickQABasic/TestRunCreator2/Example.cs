﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestRunCreator
{
    public partial class TestRunCreatorClass2
    {
        public static void Example(string address = "10.1.24.50:8080")
        {
            // Create the SlickReporting connection, this is your Slick server ip address
            SlickQABasic.SlickReporting slickBasic = new SlickQABasic.SlickReporting(address);
            
            // Create the TestRunObject
            TestRunCreator.TestRunCreatorClass2 slickTestRun = new TestRunCreator.TestRunCreatorClass2(slickBasic);
            // Create the TestRun - Build and Release data must be valid, i.e. not null or ""
            slickTestRun.CreateAndPostTestRun("Example", "Smoke Example", "Smoke Example", "1.1", "1.0.10001", "COMPUTER");
            
            // Upload a file to the TestRun, these are uploaded immediately as we need a reference to the file for the TestRun
            slickTestRun.AddFilesToTestRun("Picture.jpg", "Picture.txt");
            // Add an example file from memory
            SlickQAClasses.StoredFile exampleFile = TestRunCreator.TestRunCreatorClass2.AddFilesFromMemoryString(slickBasic, "Example of a text file.", "Example.txt");
            
            // Create the log list
            List<SlickQAClasses.ResultClass.LogEntry> logList = new List<SlickQAClasses.ResultClass.LogEntry>();
            // Create some example log entries
            logList.Add(slickTestRun.CreateLogEntry("Hello World 1"));
            logList.Add(slickTestRun.CreateLogEntry("Hello World 2"));
            
            // Create the file list for the slick test case
            List<FileClass> fileClassList = new List<FileClass>()
                {
                    new FileClass("Picture.jpg", "Picture.jpg")
                };
            fileClassList.Add(new FileClass("", exampleFile.filename) { id = exampleFile.id });

            // Create the TestCase Result, notice the "null" below, null is the default response when you don't have something
            slickTestRun.AddSlickTestCaseResult(
                "TestCase Example 1",
                DateTime.Now,
                DateTime.Now,
                logList,
                SlickStringResults.pass,
                fileClassList,
                null,
                "Development Test Station"
            );

            /// Create another TestCase Result, notice the additional "null" data below
            /// This will allow for some parts to be used, but not others as desired
            slickTestRun.AddSlickTestCaseResult(
                "TestCase Example 2",
                DateTime.Now,
                DateTime.Now,
                null,
                SlickStringResults.fail,
                null,
                "Intentionally failed this example",
                "Development Test Station"
            );

            // Create a name for the report with "example" at the end
            slickTestRun.ReportCreateFileName("example");
            // Write it out for reference
            slickTestRun.ReportWrite();
            // POST the TestRun to Slick, be sure the Build and Release information are correct by this stage
            slickTestRun.UpdateAndFinishTestRun();
        }
    }
}