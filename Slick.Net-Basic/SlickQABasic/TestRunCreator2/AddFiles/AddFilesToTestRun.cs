﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

namespace TestRunCreator
{
    /// <summary>
    /// Consider this the top level class to adding a testrun with results to slick.
    /// Everything in this class is needed in order for slick to look "right".
    /// </summary>
    public partial class TestRunCreatorClass2
    {
        /// <summary>
        /// Here we add files to the test run
        /// </summary>
        public SlickQAClasses.StoredFile AddFilesToTestRun(string fileLocation, string slickFileName)
        {
            // Return our file from slick
            SlickQAClasses.StoredFile slickFile = new SlickQAClasses.StoredFile();

            // The automation deployment system file local
            System.IO.FileInfo file = new System.IO.FileInfo(fileLocation);
            // POST the file
            if (file.Exists)
            {
                SlickQABasic.SlickReporting.JsonResultsClass posted = slickBasic.PostFile(file, slickFileName, GrabMimeType(System.IO.Path.GetExtension(fileLocation)));

                // Check that our list is not null
                if (TestRunActual.files == null)
                {
                    TestRunActual.files = new List<SlickQAClasses.StoredFile>();
                }

                // Now add the file POST result
                TestRunActual.files.Add((SlickQAClasses.StoredFile)posted.localObject);
                slickFile = (SlickQAClasses.StoredFile)posted.localObject;
            }

            return slickFile;
        }

        public void AddFilesToTestRun(SlickQAClasses.StoredFile file)
        {
            if (file != null)
            {
                if (TestRunActual.files == null)
                {
                    TestRunActual.files = new List<SlickQAClasses.StoredFile>();
                }

                TestRunActual.files.Add(file);
            }
        }
    }
}