﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using Newtonsoft.Json;

namespace SlickQAClasses
{
    /// <summary>
    /// Pulled from:
    /// http://www.slickqa.com/apidocs.html#!/testcases
    /// Primary Class is: Testcase
    /// </summary>
    public class TestCaseClass
    {
        public class Testcase
        {
            public List<RecurringNote> activeNotes;
            public string author;
            public bool automated;
            public string automationConfiguration;
            public string automationId;
            public string automationKey;
            public Int64 automationPriority;
            public string automationTool;
            public ComponentReference component;
            public bool deleted;
            public FeatureReference feature;
            public string id;
            public Int64 importanceRating;
            public List<RecurringNote> inactiveNotes;
            public string name;
            public ProjectReference project;
            public string purpose;
            public string requirements;
            public Int64 stabilityRating;
            public List<Step> steps;
            public List<string> tags;
        }

        public class RecurringNote
        {
            public ConfigurationReference environment;
            public string message;
            public ReleaseReference release;
            public string url;
        }

        public class ConfigurationReference
        {
            public string configId;
            public string filename;
            public string name;
        }
        public class ReleaseReference
        {
            public string name;
            public string releaseId;
        }
        public class ComponentReference
        {
            public string code;
            public string id;
            public string name;
        }
        public class FeatureReference
        {
            public string id;
            public string name;
        }

        public class Step
        {
            public string expectedResult;
            public string name;
        }
    }
}