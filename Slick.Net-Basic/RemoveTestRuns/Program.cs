﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoveTestRuns
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the ip address and port number if any of the slick server: ");

            SlickQABasic.SlickReporting slickBasic = new SlickQABasic.SlickReporting(Console.ReadLine());
            SlickQABasic.Example example = new SlickQABasic.Example();
            example.RunSimpleSlickTest("10.1.24.66:8080");

            Console.WriteLine("\r\nEnter the project name: ");
            string projectString = Console.ReadLine();

            //Console.WriteLine("\r\nEnter the testplan name: ");
            //string testplanString = Console.ReadLine();

            SlickQABasic.SlickReporting.JsonResultsClass project = slickBasic.GetProject(projectString);
            SlickQAClasses.ProjectReference projectReference = new SlickQAClasses.ProjectReference() { id = project.ObjectToProject().id, name = project.ObjectToProject().id };
            //SlickQAClasses.TestPlan testplan = new SlickQAClasses.TestPlan();
            //SlickQABasic.SlickReporting.GetTestPlanResults testplans = slickBasic.GetTestPlan();
            //for (int i = 0; i < testplans.testplan.Count; i++)
            //{
            //    if (testplans.testplan[i].name == testplanString)
            //    {
            //        testplan = testplans.testplan[i];
            //    }
            //}

            SlickQABasic.SlickReporting.JsonResultsClass testruns = slickBasic.GetTestRuns();

            Console.WriteLine("\r\nTestRuns, Here we are going to ask if you want to remove a test run\r\nIf you do, enter \"Yes\" and press enter after each testrun on the console.\r\n");
            for (int i = 0; i < testruns.ObjectToGetTestRuns().Count; i++)
            {
                Console.WriteLine(String.Format("name: {0}, testplan: {1}\r\n  no_result: {2}\r\n  pass: {3}\r\n  fail: {4}\r\n  broken_test: {5}\r\n  not_tested: {6}\r\n  skipped: {7}\r\n", testruns.ObjectToGetTestRuns()[i].name, testruns.ObjectToGetTestRuns()[i].testplan.name, testruns.ObjectToGetTestRuns()[i].summary.resultsByStatus.NO_RESULT, testruns.ObjectToGetTestRuns()[i].summary.resultsByStatus.PASS, testruns.ObjectToGetTestRuns()[i].summary.resultsByStatus.FAIL, testruns.ObjectToGetTestRuns()[i].summary.resultsByStatus.BROKEN_TEST, testruns.ObjectToGetTestRuns()[i].summary.resultsByStatus.NOT_TESTED, testruns.ObjectToGetTestRuns()[i].summary.resultsByStatus.SKIPPED));

                Console.WriteLine("Remove TestRun?");
                string removeValue = Console.ReadLine();

                if(removeValue == "Yes" | removeValue == "yes")
                {
                    SlickQAClasses.TestrunReference reference = new SlickQAClasses.TestrunReference()
                    {
                        name = testruns.ObjectToGetTestRuns()[i].name,
                        testrunId = testruns.ObjectToGetTestRuns()[i].id
                    };
                    slickBasic.DeleteTestRun(reference);

                    Console.WriteLine("Deleting testrun: {0}\r\n", reference.testrunId);
                }
            }

            Console.WriteLine("Press \"Enter\" to quit");
            Console.ReadLine();
        }
    }
}