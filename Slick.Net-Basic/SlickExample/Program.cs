﻿/**************************************************************************
Copyright (c) 2015, Chad Capson khilghard@gmail.com
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.
***************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlickExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // Example of Project POST
            // Example1("Example");

            // Example of TestRunCreator
            TestRunCreator.TestRunCreatorClass.Example();
        }

        /// <summary>
        /// Creates and "Example" project in Slick
        /// </summary>
        /// <param name="projectName"></param>
        /// <returns></returns>
        public static SlickQAClasses.ProjectReference Example1(string projectName)
        {
            // Connect to slick
            SlickQABasic.SlickReporting slickBasic = new SlickQABasic.SlickReporting("10.1.24.50:8080");

            // Create the basic project with a name
            SlickQAClasses.ProjectsClass.Project project = new SlickQAClasses.ProjectsClass.Project()
            {
                  name = projectName
            };
            // Post this new project to Slick
            SlickQABasic.SlickReporting.JsonResultsClass results = slickBasic.PostProject(project);
            // Create a reference to the project from the Project object
            SlickQAClasses.ProjectReference projectRef = new SlickQAClasses.ProjectReference()
            {
                id = results.ObjectToProject().id,
                name = results.ObjectToProject().name
            };

            // Return the reference only
            return projectRef;
        }
    }
}
