---
# Slick.Net Readme #
---

This report mechanism was created to gather test results for software automation frameworks. It is a client library for SlickQA: https://github.com/slickqa. The old .Net client was not compileable in VS2013 and so this project was created. It is supposed to be very simple. The projects are as follows:

SlickExample

* An example for TestRunCreator or just using the slick classes

SlickQABasic

* Contains the basic structure to POST, PUT, and GET results from slick
* TestRunCreator - This is the easy, no frills, just create a TestRun and POST the results
* SlickQAClasses - These are the class objects for the slick api mechanism
* SlickQABasic - The POST, PUT, GET methods and basic slick setup classes
   
For example code look in:

* Slick.Net-Basic\SlickExample\Program.cs
* Slick.Net-Basic\SlickQABasic\TestRunCreator\Example.cs
* Slick.Net-Basic\SlickQABasic\SlickReporting\Example.cs

---
# SlickQABasic Reporting #
---

This library uses the swagger classes from SlickQA from http://www.slickqa.com/apidocs.html. The only implemented classes are the basics needed to start POSTing results to slick. Those classes are: projects, results, testcases, testplans, and testruns.

In order to push results to slick:

1. Create a project and store the id or get the project name and store the id
2. Create/get the latest release and create the release reference id/data
3. Create/get the lastest build and create the build reference id/data
4. Create/get the testplan and build the testplan reference id/data
5. Create/get the testcase (TestCase Results) and build the testcase reference id/data
6. Create a testrun and store the project, release, build, and testcase references and push to slick
7. Create a result and reference the project, testcase, release, build, and testrun and push to slick
    * One thing to note here is that some of the fields are required. Status is one of them read on below for what to use in that field. The result adds log entries. If you add multiple result classes to slick it will add all log entries in one row. If you click on that row, you will get the log entries from that one result. Multiple results will mean multiple log entries. So tests can be broken down that way when reporting results.


The above steps can be done with curl:

    curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://192.168.0.116/slick/api/projects/byname/Panel

    curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://192.168.0.116/slick/api/testcases

    curl -X POST -H "Accept: Application/json" -H "Content-Type: application/json" http://192.168.0.116/slick/api/testcases -d '{"author":"Chad Capson","automated":true,"automationPriority":0,"deleted":false,"importanceRating":0,"name":"Update 1.1.2 to 2.0.0","project":{"name":"Panel"},"purpose":"Store results for all 1.1.2 to 2.0.0 testing","stabilityRating":0}'

    curl -X POST -H "Accept: Application/json" -H "Content-Type: application/json" http://192.168.0.116/slick/api/testruns -d '{\"runStarted\": 0, \"files\": [], \"name\": \"Test: Panel Project; TestCase: Update 1.1.2 to 2.0.0; 2015-04-05 22:02:36\", \"dateCreated\": 0, \"project\": {\"id\": \"551fedaca6a27f76cdf189d1\", \"name\": \"Panel Project\"}, \"build\": {\"name\": \"1.1.2.10784\"}, \"runFinished\": 0, \"summary\": {\"totalTime\": 0, \"statusListOrdered\": [], \"total\": 0, \"resultsByStatus\": {\"SKIPPED\": 0, \"BROKEN_TEST\": 0, \"NO_RESULT\": 0, \"NOT_TESTED\": 0, \"PASS\": 0, \"FAIL\": 0}}, \"id\": \"55220557a6a27f5fafc1ddeb\"}'

    curl -X POST -H "Accept: Application/json" -H "Content-Type: application/json" http://192.168.0.116/slick/api/results -d '{"build":{"name":"1.1.2.10784"},"finished":0,"hostname":"LAP-CCAPSON","log":[{"entryTime":0,"loggerName":"Entry","message":"Hello World 1"},{"entryTime":0,"loggerName":"Entry","message":"Hello World 2"}],"project":{"name":"Panel"},"recorded":0,"runlength":0,"runstatus":"Passed","started":0,"testcase":{"name":"Update 1.1.2 to 2.0.0"},"testrun":{"name":"2015-04-03"}}'

    curl -i -X PUT -H "Content-Type:application/json" http://192.168.0.44/slick/api/testruns/5554f712f7d75c0c1004772e -d '{"runStarted": 0, "files": [{"mimetype": "image/jpeg", "chunkSize": 262144, "filename": "Picture.jpeg", "length": 7490232, "uploadDate": 1431631699495, "id": "5554f703f7d75c0c1004771e"}], "testplanId": "5538353ef7d75c0e83abbc6c", "name": "Slick Smoke Test", "dateCreated": 1431631715547, "project": {"name": "Panel", "id": "55359740f7d75c0991aca714"}, "build": {"buildId": "55383452f7d75c0e83abbc45", "name": "1.1.2.10000"}, "release": {"releaseId": "55383452f7d75c0e83abbc44", "name": "1.1.2"}, "runFinished": 0, "summary": {"totalTime": 0, "statusListOrdered": ["PASS"], "total": 2, "resultsByStatus": {"SKIPPED": 0, "BROKEN_TEST": 0, "NO_RESULT": 0, "NOT_TESTED": 0, "PASS": 2, "FAIL": 0}}, "id": "5554f712f7d75c0c1004772e", "testplan": {"name": "Panel Smoke Test 2", "project": {"name": "Panel", "id": "55359740f7d75c0991aca714"}, "sharedWith": [], "createdBy": "chad.capson", "isprivate": false, "id": "5538353ef7d75c0e83abbc6c"}}'

    curl -X DELETE "https://192.168.56.101/slick/api/testruns/5739ec704b2de46bb9438f11"

# Dll Usage #
Add a reference to the .dll or the project source and add this to a method somewhere:

SlickQAClasses.SlickQABasic.Example.RunSetup("127.0.0.1");

This does assume that you have already created a project somewhere, it's not something I'm doing in my code so that the slick admin will create only what we want/need for projects and then gives us the project list. So I don't create projects. Also make sure to change the ip address to the ip address of your slick server, just FYI, in case that wasn't already clear. This also assumes you have a project named: Panel as well.

A quick note on passing/failing a testrun The result api requires a status string parameter, I wanted to cover it briefly in the readme. The elements are PASS, FAIL, BROKENTEST, NORESULT, NOTTESTED, and SKIPPED. Use one of these in the TestRunsClass.Testruns.status variable when pushing a testrun to slick. Slick will push back if you don't have certain variables set in the json so make sure to use those. Use the curl commands to verify that things are going to work. It's what I did to verify that slick was going to work and then move to .net for the  implementation.

# Dependencies #
Newtonsoft.Json
I used Newtonsoft.Json because it's proven to be a very useful third party tool for 
doing json with my Linux systems. So that is definitely a need for this dll.

# RestClient #
I also lifted the RestClient code from a codeproject article. It's been real useful. So kudos to the author, it's working very well. Lifted from http://www.codeproject.com/Tips/497123/How-to-make-REST-requests-with-Csharp

# Slick Apis #
I used the swagger output from http://www.slickqa.com/apidocs.html which allowed me to just copy the api classes into my .net classes and fix the output. I know I could have used lots of other things, but it took me less time to copy and paste what I needed than to add all sorts of swagger stuff to my system and convert the url swagger to c# code.


# Query Commands #
The query structure is not documented. Here is what I got from Jason, the creator of slick:

The q parameter is not just any text.  It’s a query language.  However you shouldn’t need it, most of your queries are probably going to be much simpler.  Any property can be queried for in the url.  So if you are looking for any test runs with Panel as the name it would be /api/testruns?name=Panel. If you want to look for any test runs that belong to the Panel project it would be /api/testruns?project.name=Panel. The q parameter allows for more complex queries (like OR’s and NOT’s).

So instead of ?q= use ? and the class structure from slick. It's not documented but hopefully this explanation will prove helpful.

# E-Mail #

if you need to add amqp support to slick (which you will) here is a sample:

> mongo localhost/slick
> db[‘system-configurations’].insert({
> 	"name" : "AMQP System Configuration",
> 	"className" : "org.tcrun.slickij.api.data.AMQPSystemConfiguration",
> 	"configurationType" : "amqp-system-configuration",
> 	"exchangeName" : "amq.topic",
> 	"hostname" : "localhost",
> 	"port" : 5672
> })


On May 18, 2015, at 5:08 PM, Jason Corbett <jason.corbett@vivint.com> wrote:

Getting slick to send emails takes more than a basic install.  You have to have 
a couple more things installed and configured.

See https://code.google.com/p/slickqa/wiki/Narc

Alternately, I copied that page to this repo under this filename: slick_e-mail_setup.txt

Jason


# TestRunCreator #
Created a TestRunCreator class. This class holds the entire Slick TestRun. One of the key design concepts is posting the testrun and testcase results at the end of the run vs adding items as the test is run. Some tests take a very long time to run and the data in slick can be gathered from other means such as a debug log. So the primary aim of this dll and TestRunCreatorClass is for storage of the data. The data can then be uploaded all at once at the end of a run. It's a choice of philosophy to post as you go or all at once and each way has its own merits. I prefer all at the end so the entire test gets posted all at once. Failures are dealt with by the program calling the test. In this manner, the entire test is gathered and can be uploaded in one shot. It can also be serialized into xml data as the test goes on. If the test program should terminate early, the data from each test case run would still be available for use. It would also be easy to fork the project and make the test post as it goes. Either approach is acceptable and is simply a matter of test philosophy.

An example of the TestRunCreator is here:

TestRunCreator.TestRunCreatorClass.Example("127.0.0.1");